console.log(`
*==============*
| Hello World! |
*==============*`)

// Arrow Functions

// Old Version Ex1:
function sum(num1, num2) {
  return num1 + num2
}

// ES6
const sumV2 = (num1, num2) => num1 + num2

// Old Version Ex2:
function isPositive(num) {
  return num >= 0
}

// ES6 - don't do this; unreadable | do this instead => (num)
const isPositiveV2 = (num) => num >= 0

// Old Version Ex3:
function randNum() {
  console.log(Math.random())
}

// ES6
const randNumV2 = () => Math.random()

/*
Create an object to hold information
on your favorite recipe. It should 
have properties for title (a string), 
servings (a number), and ingredients 
(an array of strings).
*/

const recipe = {
  title: 'Rice ala perfection',
  servings: 5,
  ingredients: ['one cup of rice', 'two cups of water', 'a pinch of salt'],
  feedback: () => console.log('Delicious!')
}

recipe.feedback()

/*Create an array of objects, where each object
 describes a book and has properties for the 
 title (a string), author (a string), and 
 alreadyRead (a boolean indicating if you 
read it yet).
*/

// const germany = {
//     title: 'Mein Kamf',
//     author: 'Adolf Hitler',
//     alreadyRead: true
// }
// const philippines = {
//     title: 'La Soledaridad',
//     author: 'Jose Rizal',
//     alreadyRead: true
// }
// const china = {
//     title: 'The Private Life of Chairman Mao',
//     author: 'Li Zhishui',
//     alreadyRead: false
// }
// const [ germany, philippines, china ] = influentialBooksOfTheWorld

const books = [
  {
    title: 'Mein Kamf',
    author: 'Adolf Hitler',
    alreadyRead: true,
    rating: 5
  },
  {
    title: 'La Soledaridad',
    author: 'Jose Rizal',
    alreadyRead: true,
    rating: 1
  },
  {
    title: 'The Private Life of Chairman Mao',
    author: 'Li Zhishui',
    alreadyRead: false,
    rating: 4
  }
]

// influentialBooksOfTheWorld.forEach(influentialBooksOfTheWorld => console.log(influentialBooksOfTheWorld.title))
// influentialBooksOfTheWorld.forEach(influentialBookOfTheWorld => {
//     if (influentialBookOfTheWorld.alreadyRead === false)
//         console.log(influentialBooksOfTheWorld.title)

// }

// for(i = 0; i <= influentialBooksOfTheWorld.length; i++) {
//     if (influentialBooksOfTheWorld.alreadyRead === false)
//         console.log(influentialBooksOfTheWorld.title)
// }

// *
// for(i = 0; i < books.length; i++){
//     if (books[i].alreadyRead === false)
//         console.log(books[i].title)
// }

for (i = 0; i < books.length; i++) {
  if (books[i].rating >= 3)
    console.log(`Title: ${books[i].title}, Rating: ${books[i].rating}`)
}

// let person = {
//     firstName: 'Marshall',
//     lastName: 'Law',
//     fullname: `${this.lastName}, ${this.firstName}`,
//     address: {
//         city: 'Iwata',
//         prefecture: 'Shizuoka',
//         country: 'Japan'
//     },
//     greeting: () => `Konnichiwa, watashi no namae wa ${person.firstName} ${person.lastName}`,
//     greetingPlace: () => `${person.address.prefecture}, ${person.address.country}`
// }

// let personA = new person();

const myGrades = {
  science: 98,
  math: 87,
  english: 81,
  programming: 99
}

// topScore = (myGrades) => Math.max(myGrades)
for (let key of Object.keys(myGrades)) temp = myGrades[key]
console.log(Math.max(temp))

// num = num -1
// Thus either num + 1
// Previous Version:
// function randomNum(num) {
//     if (num === undefined)
//         num = 6
//     return Math.floor(Math.random() * num + 1)
// }
// ES6
function randomNum(num = 6) {
  return Math.floor(Math.random() * num + 1)
}
// Arrow function
const randomNumV3 = (num = 6) => Math.floor(Math.random() * num + 1)

function roundNum() {
  return Math.round(134.5789)
}
