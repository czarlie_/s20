console.log(
`*=============*
| Hello World |
*=============*`)

let movies = ["Godfather", 'Mission Impossible', "Avengers", 'The Nun']

// console.log(movies[0])
// console.log(movies[1])
// console.log(movies[2])

// movies[1] = 'Halloween'

// console.log(movies[1])

// console.log('******')

// let colorsNew = []
// let colorsOld = new Array()

// let random_collection = [49, true, 'Hermione', null]

// let nums = [45, 37, 89, 24]
// nums.length

// console.log('******')

// console.log(movies[1])
// console.log(movies.length)
// console.log(movies.lastIndexOf)
// console.log(movies.length-1)

// console.log('******')

// array.methods()
// ==========
// let colors = ['red', 'orange', 'yellow']
// console.log(`Original Array: ${colors}`)

// colors.push('green')
// console.log(`push(): ${colors}`)

// let popped = colors.pop()
// console.log(`popped(): ${popped}`)
// console.log(`pop(): ${colors}`)

// colors.unshift('infared')
// console.log(`unshift(): ${colors}`)

// console.log(colors.shift())
// console.log(`shift(): ${colors}`)

// console.log(`+=====+`)
// let tuitt = ['Charles', 'Paul', 'Sef', 'Alex', 'Paul']

// console.log(tuitt.indexOf('Sef'))
// console.log(tuitt.indexOf('Paul'))
// console.log(tuitt.indexOf('Hulk'))

// let colors = ['red', 'orange', 'yellow', 'green', 'black']
// for(let i = 0; i < colors.length; i++)
//     console.log(`${colors[i]} is in ${[i]}`)

// Wrong
// colors.forEach((colors) => {
//     console.log(`${colors} is in ${colors}`)
// })

// colors.forEach(function(color) {
//     console.log(color)// color is a placeholder
// })

// color.forEach(color) => {
// console.log(color)}

// colors.forEach((color) => {
//     console.log(color)
// })

// *****
// let colors = ['red', 'green', 'blue']
// let colors2 = ['red', 'green', 'blue']
// let colors3 = colors;

// const scores = [
//     [12, 15, 18],
//     [11, 00, 98],
//     [69, 5, 6]
// ]
// console.log(scores)
// for(i = 0; i < scores.length; i++)
//     // console.log(scores[i])
//         for(j = 0; j < scores[i].length; j++)
//             console.log(scores[i][j])

// scores.forEach((score) => {
//     console.log(score)
// });

// **********
const grades = [98, 87, 91]

const myGrades = {
    lastName: 'Law',
    firstName: 'Marshall',
    fullname() {
        return (`${myGrades.lastName}, ${myGrades.firstName}`)
    },
    science: 98,
    math: 87,
    english: 91,
    programming: 84,
    hobbies: ['cooking', 'martial arts', 'family bonding']
}

console.log(`${myGrades.fullname()}'s hobbies: ${myGrades.hobbies}`)

const person = {
    lastName: 'Law',
    firstName: 'Forest',
    fullName() {
        return `${person.lastName}, ${person.firstName}`
    },
    email: ['forestlaw@gmail.com', 'flaw@protonmail.com'],
    address: {
        city: 'Shizuoka',
        country: 'Japan'
    },
    greeting() {
        return 'hi'
    }
}
console.log(`${person.fullName()}'s address: ${person.address} and email: ${person.email}`)
console.log(person.greeting())
console.log(person.address)

console.log('**********')

const shoppingCart = [ {
        product: 'shirt',
        price: 99.90,
        discount: 10,
        quantity: 2 },
    {   
        product: 'watch',
        price: 999.90,
        quantity: 2 
    },
    {   product: 'laptop',
        price: 9909.90,
        quantity: 3
    }
]

const blogger = {
    lastName: 'Smith',
    firstName:'John',
    posts: [
        {
            title: 'my first post',
            body: 'lorem ipsum',
            comments: [
                {}
            ]
        },
        {

        },
        {}
    ]
}